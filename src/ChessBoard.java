import  java.util.Arrays;

public class ChessBoard {

    char[][] board;
    int n;
    int nMinusOne;
    int numberOfSolutions = 0;

    ChessBoard(int n){
        this.n = n;
        this.board = new char[n][n];
        this.nMinusOne = n-1;

        for (char[] row: board)
            Arrays.fill(row, '-');
    }

    public char[][] getBoard() {
        return board;
    }

    public boolean checkRow(char[][] board, int row){
        for(int i = 0; i <= nMinusOne; i++) {
            if(board[row][i] == 'Q'){
                return false;
            }
        }
        return true;
    }

    public boolean checkCol(char[][] board, int col){
        for(int i = 0; i <= nMinusOne; i++) {
            if(board[i][col] == 'Q'){
                return false;
            }
        }
        return true;
    }

    public boolean checkUpperDiagonal(char[][] board, int row, int col){
        for (int i = row, j = col; i >= 0 && j >= 0; i--, j--) {
            if(board[i][j] == 'Q'){
                return false;
            }
        }
        for (int i = row, j = col; i <= nMinusOne && j <= nMinusOne; i++, j++) {
            if(board[i][j] == 'Q'){
                return false;
            }
        }
        return true;
    }

    public boolean checkLowerDiagonal(char[][] board, int row, int col){
        for (int i = row, j = col; i <= nMinusOne && j >= 0; i++, j--) {
            if(board[i][j] == 'Q'){
                return false;
            }
        }
        for (int i = row, j = col; i >= 0 && j <= nMinusOne; i--, j++) {
            if(board[i][j] == 'Q'){
                return false;
            }
        }
        return true;
    }

    public boolean checkPosition(char[][] board, int row, int col){
        return checkRow(board, row) &&
                checkCol(board, col) &&
                checkUpperDiagonal(board, row, col) &&
                checkLowerDiagonal(board, row, col);
    }
    public void setStartPosition(int row, int col){
        board[row][col] = 'Q';
    }

    public boolean solve(char[][] board, int row, int col, int count){

        if (row > nMinusOne || col > nMinusOne){
            if (count == n) {
                numberOfSolutions++;
                printBoard(board, n);
                return true;
            }
            else{
                return false;
            }
        }

        if(checkPosition(board, row, col)) {
            board[row][col] = 'Q';
            if(row<nMinusOne) {
                solve(board, row+1, col, count+1);
            }else{
                solve(board, 0,  col+1, count +1);
            }
            board[row][col] = '-';
        }

        if(row<nMinusOne) {
            solve(board, row+1, col, count);
        }else{
            solve(board, 0,  col+1, count);
        }
        return false;
    }

    static void printBoard(char[][] board, int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public int getNumberOfSolutions() {
        return numberOfSolutions;
    }

    public void setNumberOfSolutions() {
        numberOfSolutions = 0;
    }

    public void clearStartPosition(int row, int col) {
        board[row][col] = '-';
    }
}
