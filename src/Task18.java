import java.util.Scanner;

class NQueen {
    public static void main(String[] args) {
        runSolutions();
    }

    public static void runSolutions(){
        final String regex = "[ABCDEFGH][12345678]";
        ChessBoard chess = new ChessBoard(8);
        int row = 0;
        int col = 0;
        int count = 0;
        String input;
        Scanner mySc = new Scanner(System.in);
        Scanner coordinateScan = new Scanner(System.in);

        System.out.println("Please select one of the menus to perform the 8-queens problem");
        System.out.println("1) Display all solutions\n2) Select the position of the first queen\n3) Quit");
        while(mySc.hasNext()){
            input = mySc.next();

            switch(input){
                case "1":
                    count = 0;
                    chess.solve(chess.getBoard(), 0, 0, count);
                    System.out.println("Found solution(s): "+chess.getNumberOfSolutions());
                    chess.setNumberOfSolutions();
                    break;
                case "2":
                    System.out.println("Please provide a coordinate in algebraic notation");

                    while(coordinateScan.hasNext()){
                        String coordinate = coordinateScan.next().toUpperCase();
                        if(!coordinate.matches(regex)){
                            System.out.println("Wrong coordinate format, please enter coordinate in algebraic notation!");
                            System.out.println("Example: A1");
                        }else {
                            count = 1;
                            row = 8 - (coordinate.charAt(1) - '0');
                            col = coordinate.charAt(0) - 'A';
                            chess.setStartPosition(row, col);
                            chess.solve(chess.getBoard(), 0, 0, count);
                            chess.clearStartPosition(row, col);
                            System.out.println("Found solution(s): "+chess.getNumberOfSolutions());
                            chess.setNumberOfSolutions();
                            break;
                        }
                    }
                    break;
                case "3":
                    mySc.close();
                    coordinateScan.close();
                    System.exit(0);
                    break;
                default:
                    System.out.println("Please enter one of the options provided");

                break;
            }
            System.out.println("1) Display all\n2) Select the first coordinate\n3) Quit");
        }
    }
}